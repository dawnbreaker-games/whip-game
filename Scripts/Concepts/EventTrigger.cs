using Godot;
using System;

public partial class EventTrigger : Node2D
{
	[Export]
	public uint maxTriggers;
	[Export]
	public bool triggerOnBodyEnter;
	[Export]
	public CanvasItem[] toggleCanvasItemsOnTrigger = new CanvasItem[0];
	[Export]
	public Area2D area;
	uint triggerCount;
	
	public override void _Ready ()
	{
		TreeExiting += () => { area.BodyEntered -= OnBodyEntered; };
		if (triggerCount < maxTriggers)
		{
			if (triggerOnBodyEnter)
				area.BodyEntered += OnBodyEntered;
		}
	}
	
	void OnBodyEntered (Node2D node)
	{
		TryTrigger ();
	}
	
	void TryTrigger ()
	{
		if (triggerCount >= maxTriggers)
		{
			area.BodyEntered -= OnBodyEntered;
			return;
		}
		for (int i = 0; i < toggleCanvasItemsOnTrigger.Length; i ++)
		{
			CanvasItem toggleCanvasItem = toggleCanvasItemsOnTrigger[i];
			toggleCanvasItem.Visible = !toggleCanvasItem.Visible;
		}
		triggerCount ++;
	}
}
