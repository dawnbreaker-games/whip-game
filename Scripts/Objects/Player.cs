using Godot;
using System;
using Extensions;

namespace FightRoom
{
	public partial class Player : Node2D, IDestructable
	{
		public float Hp
		{
			get
			{
				return hp;
			}
			set
			{
				hp = value;
			}
		}
		public uint MaxHp
		{
			get
			{
				return maxHp;
			}
			set
			{
				maxHp = value;
			}
		}
		[Export]
		public uint maxHp;
		[Export]
		public CharacterBody2D characterBody;
		[Export]
		public float moveSpeed = 100;
		float hp;
		bool dead;
		float yVelocity;
		static float gravity;
		
		public override void _Ready ()
		{
			hp = maxHp;
			gravity = (float) ProjectSettings.GetSetting("physics/2d/default_gravity");
		}
		
		public override void _PhysicsProcess (double deltaTime)
		{
			float input = Input.GetAxis("Move left", "Move right");
			characterBody.Velocity = characterBody.Velocity.SetX(input * moveSpeed);
			if (input != 0)
			{
				if (input < 0)
				{
					characterBody.Scale = new Vector2(Mathf.Sign(input), -1);
					characterBody.RotationDegrees = 180;
				}
				else
					characterBody.Scale = new Vector2(Mathf.Sign(input), 1);
			}
			yVelocity += gravity * (float) deltaTime;
			characterBody.Velocity = characterBody.Velocity.SetY(yVelocity);
			characterBody.MoveAndSlide();
		}
		
		public void TakeDamage (float amount)
		{
			if (dead)
				return;
			hp = Mathf.Clamp(hp - amount, 0, maxHp);
			if (hp == 0)
				Death ();
		}
		
		public void Death ()
		{
			dead = true;
		}
	}
}
