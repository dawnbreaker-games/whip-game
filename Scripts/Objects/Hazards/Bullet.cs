using Godot;
using System;
using Extensions;

namespace FightRoom
{
	public partial class Bullet : Hazard
	{
		[Export]
		public float moveSpeed;
		[Export]
		public RigidBody2D rigid;
		
		public override void _Ready ()
		{
			base._Ready ();
			rigid.LinearVelocity = VectorExtensions.FromFacingAngle(RotationDegrees) * moveSpeed;
		}
	}
}
