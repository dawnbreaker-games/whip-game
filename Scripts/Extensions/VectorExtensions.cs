using Godot;

namespace Extensions
{
	public static class VectorExtensions
	{
		public static float GetFacingAngle (this Vector2 v)
		{
			return Mathf.RadToDeg(Mathf.Atan2(v.Y, v.X)) + 90;
		}
		
		public static Vector2 FromFacingAngle (float angle)
		{
			angle = Mathf.DegToRad(angle);
			return new Vector2(Mathf.Cos(angle), Mathf.Sin(angle));
		}
		
		public static Vector2 SetX (this Vector2 v, float x)
		{
			return new Vector2(x, v.Y);
		}
		
		public static Vector2 SetY (this Vector2 v, float y)
		{
			return new Vector2(v.X, y);
		}
	}
}
