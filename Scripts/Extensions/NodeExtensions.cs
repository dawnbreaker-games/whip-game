using Godot;
using System;

namespace Extensions
{
	public static class NodeExtensions
	{
		public static void ApplyMetadata<T> (this Node node, ref T value)
		{
			Type valueType = value.GetType();
			if (valueType.IsArray)
			{
				uint index = 0;
				Array array = default(Array);
				string valueNamePrefix = nameof(value) + "_";
				while (node.HasMeta(valueNamePrefix + index))
				{
					Type? elementType = valueType.GetElementType();
					if (elementType.IsAssignableTo(typeof(Node)))
					{
						array.SetValue(node.GetTree().CurrentScene.GetNode<Node>((string) node.GetMeta(valueNamePrefix + index)), index);
						index ++;
					}
				}
				value = (T) (object) array;
			}
			else
				value = (T) (object) node.GetMeta(nameof(value), (Variant) (object) value);
		}
		
		public static T GetParentNode<T> (this Node node, bool scanAllParents = true) where T : Node
		{
			T output = node as T;
			if (output != null)
				return output;
			else if (scanAllParents)
			{
				Node parent = node.GetParent();
				while (parent != null)
				{
					output = parent as T;
					if (output != null)
						return output;
					parent = parent.GetParent();
				}
			}
			else
				output = node.GetParent() as T;
			return output;
		}
		
		public static T GetParentInterface<T> (this Node node, bool scanAllParents = true) where T : Interface
		{
			T output = default(T);
			if (node is T)
				return (T) (Interface) node;
			else if (scanAllParents)
			{
				Node parent = node.GetParent();
				while (parent != null)
				{
					if (parent is T)
						return (T) (Interface) parent;
					parent = parent.GetParent();
				}
			}
			else
			{
				Node parent = node.GetParent();
				if (parent is T)
					return (T) (Interface) parent;
			}
			return output;
		}
	}
}
