using Godot;
using System;

namespace FightRoom
{
	public partial class GameManager : Node2D
	{
		public static GameManager instance;
		public static SceneMultiplayer sceneMultiplayer = new SceneMultiplayer();
		public static bool isServer;
		[Export]
		bool _isServer;
		public static ENetMultiplayerPeer peer = new ENetMultiplayerPeer();
		public const int PORT = 6000;
		public const string IP_ADDRESS = "100.112.20.137";
		
		public override void _Ready ()
		{
			instance = this;
			isServer = _isServer;
			sceneMultiplayer.ConnectedToServer += OnConnectedToServer;
//			sceneMultiplayer.PeerConnected += OnPeerConnected;
			if (isServer)
			{
				Error error = peer.CreateServer(PORT, 4095);
			}
			else
			{
				Error error = peer.CreateClient(IP_ADDRESS, PORT);
			}
			sceneMultiplayer.MultiplayerPeer = peer;
			GD.Print(sceneMultiplayer.MultiplayerPeer);
			GD.Print(sceneMultiplayer.IsServer());
			GD.Print(sceneMultiplayer.GetUniqueId());
		}
		
		void OnConnectedToServer ()
		{
			GD.Print("Connected to server");
		}
		
		void OnPeerConnected (int peerId)
		{
			GD.Print("Connected peer: " + peerId);
		}

		public override void _Process (double delta)
		{
		}
	}
}
